package at.aknapp.dev.autcarapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import at.aknapp.dev.autcarapp.model.ConnectionService
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController = findNavController(R.id.nav_host_fragment)

        findViewById<BottomNavigationView>(R.id.bottom_navigation_view)
            .setupWithNavController(navController)

        val appBarConfiguration = AppBarConfiguration(setOf(R.id.drivingControl, R.id.carConnection))

        setupActionBarWithNavController(navController, appBarConfiguration)

        val intent = Intent(this, ConnectionService::class.java)
        startService(intent)
    }
}