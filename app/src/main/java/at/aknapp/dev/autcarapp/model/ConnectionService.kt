package at.aknapp.dev.autcarapp.model

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.lang.Exception
import java.net.Socket
import java.nio.charset.StandardCharsets

class ConnectionService: Service() {

    private val mBinder: IBinder = ConnectionServiceBinder()

    companion object {
        var connected: Boolean = false
        var ipAddress: String = "10.0.2.2"
        var port: Int = 8090
        lateinit var socket: Socket

        var autoMode: Boolean = false
        var captureMode: Boolean = false

        suspend fun connect(connectionIp: String, connectionPort: Int): Boolean {
            Log.i("CONN", "connect to...")
            return withContext(Dispatchers.IO) {
                try {
                    // socket = Socket("10.0.2.2", 8090)
                    socket = Socket(connectionIp, connectionPort)
                    socket.soTimeout = 1000
                    socket.keepAlive = true
                    connected = true
                    ipAddress = connectionIp
                    port = connectionPort
                    true
                } catch (ex: Exception) {
                    connected = false
                    false
                }
            }
        }

        suspend fun disconnect(): Boolean {
            Log.i("CONN","disconnect from ...")
            return withContext(Dispatchers.IO) {
                try {
                    socket.close()
                    connected = false
                    true
                } catch(ex: Exception) {
                    connected = false
                    false
                }
            }
        }

        suspend fun sendCommand(command: String): Boolean {
            val json = JSONObject()
            json.put("cmd", command)
            Log.i("CONN","sending command $command")
            return withContext(Dispatchers.IO) {
                var readBuffer: ByteArray = ByteArray(4096)

                val os = socket.getOutputStream()
                os.write(json.toString().toByteArray(StandardCharsets.UTF_8))
                os.flush()

                val inputStream = socket.getInputStream()
                inputStream.read(readBuffer, 0, 20)
                Log.i("CONN", "received: ${readBuffer.toString(StandardCharsets.UTF_8)}")

                Log.i("CONN", "sended command $command")
                true
            }
        }

        suspend fun activateAutoMode(): Boolean {
            return sendCommand("auto")
        }

        suspend fun deactivateAutoMode(): Boolean {
            return sendCommand("autoOff")
        }

        suspend fun activateCaptureMode(): Boolean {
            return sendCommand("startrecording")
        }

        suspend fun deactivateCaptureMode(): Boolean {
            return sendCommand("stoprecording")
        }
    }

    override fun onCreate() {
        Log.i("CONN", "Service created...")
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.i("CONN", "Service bound...")
        return mBinder
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        Log.d("CONN", "onTaskRemoved: called.");
        stopSelf()
    }

    inner class ConnectionServiceBinder: Binder() {
        val service: ConnectionService
        get() =
            this@ConnectionService
    }


}