package at.aknapp.dev.autcarapp.drivingControl

import android.content.ComponentName
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import at.aknapp.dev.autcarapp.model.ConnectionService
import kotlinx.coroutines.launch

class DrivingControlViewModel : ViewModel() {
    private var connectionService = ConnectionService

    var autoMode = MutableLiveData<Boolean>(connectionService.autoMode)
    var captureMode = MutableLiveData<Boolean>(connectionService.captureMode)

    var mBinder = MutableLiveData<ConnectionService.ConnectionServiceBinder>()

    var serviceConnection = object: ServiceConnection {
        override fun onServiceDisconnected(component: ComponentName?) {
            Log.i("CONN", "Service disconnected from view model...")
            mBinder.postValue(null)
        }

        override fun onServiceConnected(component: ComponentName?, iBinder: IBinder?) {
            Log.i("CONN", "Service connected to view model...")
            val binder = iBinder as ConnectionService.ConnectionServiceBinder
            mBinder.postValue(binder)
        }
    }

    fun onManualCommand(){
        viewModelScope.launch {
            var result: Boolean = false
            if (connectionService.connected) {
                result = connectionService.deactivateAutoMode()
            }
            if (result) {
                connectionService.autoMode = false
                autoMode.value = connectionService.autoMode
                Log.i("CONN", "auto mode off ${connectionService.autoMode}")
            }
        }
    }

    fun onForward() {
        onManualCommand()
        var result: Boolean = false
        viewModelScope.launch {
            if (connectionService.connected) {
                Log.i("CONN", "onForward called while connected: ${connectionService.connected}")
                result = connectionService.sendCommand("fast")
            }
            if (result)
                Log.i("CONN", "command forward submitted...")
        }
    }

    fun onStop() {
        onManualCommand()
        var result: Boolean = false
        viewModelScope.launch {
            if (connectionService.connected) {
                result = connectionService.sendCommand("stop")
            }
            if (result)
                Log.i("CONN", "command stop submitted...")
        }
    }

    fun onLeft() {
        onManualCommand()
        var result: Boolean = false
        viewModelScope.launch {
            if (connectionService.connected) {
                result = connectionService.sendCommand("leftlight")
            }
            if (result)
                Log.i("CONN", "command left submitted...")
        }
    }

    fun onRight() {
        onManualCommand()
        var result: Boolean = false
        viewModelScope.launch {
            if (connectionService.connected) {
                result = connectionService.sendCommand("rightlight")
            }
            if (result)
                Log.i("CONN", "command right submitted...")
        }
    }

    fun onAutoToggle() {
        var result: Boolean
        viewModelScope.launch {
            val status = connectionService.autoMode
            if (connectionService.connected && !status) {
                result = connectionService.activateAutoMode()
                if (result) {
                    connectionService.autoMode = true
                    autoMode.value = connectionService.autoMode
                    Log.i("CONN", "auto mode $status toggled1 ${connectionService.autoMode}")
                }
            }
            if (connectionService.connected && status) {
                result = connectionService.deactivateAutoMode()
                if (result) {
                    connectionService.autoMode = false
                    autoMode.value = connectionService.autoMode
                    Log.i("CONN", "auto mode $status toggled2 ${connectionService.autoMode}")
                }
            }
            if (!connectionService.connected) {
                connectionService.autoMode = false
                autoMode.value = connectionService.autoMode
                Log.i("CONN", "auto mode $status toggled else ${connectionService.autoMode}")
            }
        }
    }

    fun onCaptureToggle() {
        var result: Boolean
        viewModelScope.launch {
            val status = connectionService.captureMode
            if (connectionService.connected && !status) {
                result = connectionService.activateCaptureMode()
                if (result) {
                    connectionService.captureMode = true
                    captureMode.value = connectionService.captureMode
                    Log.i("CONN", "capture mode $status toggled1 ${connectionService.captureMode}")
                }
            }
            if (connectionService.connected && status) {
                result = connectionService.deactivateCaptureMode()
                if (result) {
                    connectionService.captureMode = false
                    captureMode.value = connectionService.captureMode
                    Log.i("CONN", "capture mode $status toggled2 ${connectionService.captureMode}")
                }
            }
            if (!connectionService.connected) {
                connectionService.captureMode = false
                captureMode.value = connectionService.captureMode
                Log.i("CONN", "capture mode $status toggled else ${connectionService.captureMode}")
            }
        }
    }
}