package at.aknapp.dev.autcarapp.carConnection

import android.content.ComponentName
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.*
import at.aknapp.dev.autcarapp.model.ConnectionService
import kotlinx.coroutines.launch

class CarConnectionViewModel() : ViewModel() {
    private var connectionService = ConnectionService

    var connectionIp = MutableLiveData<String>(connectionService.ipAddress)
    var connectionPort = MutableLiveData(connectionService.port)
    var connected = MutableLiveData<Boolean>(connectionService.connected)
    var connectionText = MutableLiveData<String>("Connect")
    var mBinder = MutableLiveData<ConnectionService.ConnectionServiceBinder>()

    var serviceConnection = object: ServiceConnection {
        override fun onServiceDisconnected(component: ComponentName?) {
            Log.i("CONN", "Service disconnected from view model...")
            mBinder.postValue(null)
        }

        override fun onServiceConnected(component: ComponentName?, iBinder: IBinder?) {
            Log.i("CONN", "Service connected to view model...")
            val binder = iBinder as ConnectionService.ConnectionServiceBinder
            mBinder.postValue(binder)
        }
    }

    fun connectSocket() {
        var result: Boolean
        viewModelScope.launch {
            if (connectionService.connected)
                result = connectionService.disconnect()
            else
                result = connectionService.connect(connectionIp.value!!, connectionPort.value!!)
            if (result) {
                connected.value = connectionService.connected
                connectionIp.value = connectionService.ipAddress
                connectionPort.value = connectionService.port
                connectionText.value = if (connectionService.connected) "Disconnect" else "Connect"
                Log.i("CONN", "operation done...")
            } else {
                connected.value = false
                connectionText.value = "Connect"
                Log.i("CONN", "operation had errors...")
            }

        }
    }

}
